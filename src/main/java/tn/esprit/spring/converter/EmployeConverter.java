package tn.esprit.spring.converter;

import org.springframework.stereotype.Component;

import tn.esprit.spring.dto.EmployeDTO;
import tn.esprit.spring.entities.Employe;

@Component
public class EmployeConverter {

	public Employe dtoToEntity(EmployeDTO dto) {
		Employe employe = new Employe();
		employe.setPrenom(dto.getPrenomDTO());
		employe.setNom(dto.getnomDTO());
		employe.setId(dto.getIdDTO());
		employe.setEmail(dto.getemailDTO());
		employe.setRole(dto.getRole());
		employe.setActif(dto.isactifDTO());
		employe.setPassword(dto.getPassword());

		return employe;
	}
}