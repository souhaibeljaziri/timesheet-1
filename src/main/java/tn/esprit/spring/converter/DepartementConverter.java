package tn.esprit.spring.converter;

import org.springframework.stereotype.Component;

import tn.esprit.spring.dto.DepartementDto;
import tn.esprit.spring.entities.Departement;

@Component
public class DepartementConverter {

	public Departement dtoToEntity(DepartementDto dto) {
		Departement departement = new Departement();
		departement.setEmployes(dto.getEmployes());
		departement.setEntreprise(dto.getEntreprise());
		departement.setId(dto.getId());
		departement.setMissions(dto.getMissions());
		departement.setName(dto.getName());
		return departement;
	}
}
