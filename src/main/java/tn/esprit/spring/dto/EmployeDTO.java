package tn.esprit.spring.dto;

import java.util.List;

import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.entities.Timesheet;

public class EmployeDTO {

	private int id;

	private String prenomDTO;

	private String nomDTO;

	private String emailDTO;

	private String password;

	private boolean actifDTO;

	private Role roleDTO;

	private List<Departement> departements;

	private List<Timesheet> timesheets;

	private Contrat contrat;


	public EmployeDTO(String nomDTO, String prenomDTO, String emailDTO, String password, boolean actifDTO, Role roleDTO) {
		this.nomDTO = nomDTO;
		this.prenomDTO = prenomDTO;
		this.emailDTO = emailDTO;
		this.password = password;
		this.actifDTO = actifDTO;
		this.roleDTO = roleDTO;
	}

	public int getIdDTO() {
		return id;
	}



	public String getPrenomDTO() {
		return prenomDTO;
	}


	public void setPrenomDTO(String prenomDTO) {
		this.prenomDTO = prenomDTO;
	}

	public String getnomDTO() {
		return nomDTO;
	}

	
	public void setnomDTO(String nomDTO) {
		this.nomDTO = nomDTO;
	}


	public String getemailDTO() {
		return emailDTO;
	}

	public void setemailDTO(String emailDTO) {
		this.emailDTO = emailDTO;
	}




    public void setPassword(String password) {
        this.password = password;
    }



	public boolean isactifDTO() {
		return actifDTO;
	}


	public void setactifDTO(boolean actifDTO) {
		this.actifDTO = actifDTO;
	}

	public Role getRole() {
		return roleDTO;
	}

	public void setRole(Role roleDTO) {
		this.roleDTO = roleDTO;
	}

	public List<Departement> getDepartementsDTO() {
		return departements;
	}

	
	public void setDepartements(List<Departement> departementDTO) {
		this.departements = departementDTO;
	}


	 public Contrat getContrat() {
	        return contrat;
	    }

	    public void setContrat(Contrat contrat) {
	        this.contrat = contrat;
	    }

	    public List<Timesheet> getTimesheets() {
	        return timesheets;
	    }

	    public void setTimesheets(List<Timesheet> timesheets) {
	        this.timesheets = timesheets;
	    }

		public String getPassword() {
			return password;
		}



	}
