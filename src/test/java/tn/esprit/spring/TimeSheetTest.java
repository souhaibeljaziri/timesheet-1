  
package tn.esprit.spring;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.MessageFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.entities.Timesheet;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.repository.MissionRepository;
import tn.esprit.spring.repository.TimesheetRepository;
import tn.esprit.spring.services.EmployeServiceImpl;
import tn.esprit.spring.services.IEntrepriseService;
import tn.esprit.spring.services.TimesheetServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TimeSheetTest {
	
	@Autowired
	private TimesheetServiceImpl TimesheetServiceImpl;
	@Autowired
	private EmployeServiceImpl EmployeServiceImpl;
	@Autowired
	private TimesheetRepository  timesheetRepository; 
	@Autowired
	private MissionRepository  missionRepository; 
	@Autowired
	DepartementRepository departementRepository;
	@Autowired
	EmployeRepository employeRepository;
	@Autowired
    IEntrepriseService iEntrepriseService;
	
    private Mission mission;
    private Departement departement;
    private Employe employe;
    private Employe chefDepartement;
	
    private static final Logger LOG = LogManager.getLogger(TimeSheetTest.class);
	
    
    @Before
    public void setUp()
    {
        this.mission = new Mission();
        this.mission.setName("QA"); 
        this.mission.setDescription("QA");
        
        this.departement = new Departement();
        this.departement.setName("Test");

        this.employe = new Employe();
        this.employe.setNom("Ben farhat");
        this.employe.setPrenom("Aymen");
        this.employe.setEmail("bbb@gmail.com");
        this.employe.setPassword("654321");
        this.employe.setActif(true);
        this.employe.setRole(Role.INGENIEUR);

        this.chefDepartement = new Employe();
        this.chefDepartement.setNom("Ouaghlani");
        this.chefDepartement.setPrenom("Karim");
        this.chefDepartement.setEmail("aaa@gmail.com");
        this.chefDepartement.setPassword("12456");
        this.chefDepartement.setActif(true);
        this.chefDepartement.setRole(Role.CHEF_DEPARTEMENT);
        
    }
    
    @Test
    public void tests() throws ParseException {
    	ajouterMissionTest();
    	affecterMissionADepartementTest();
    	ajouterTimesheetTest();
    	validerTimesheetTest();
    	findAllMissionByEmployeJPQLTest();
    	getAllEmployeByMissionTest();
    }
    
	public void ajouterMissionTest() {
		LOG.info("Method ajouterMissionTest ");
        LOG.info(this.mission);         
		int	missionadd=TimesheetServiceImpl.ajouterMission(mission);	
		assertEquals(mission.getId(), missionadd);		
		LOG.info(this.mission + " created");
	}
	
	
	public void affecterMissionADepartementTest(){
	LOG.info("Method addMissionToDepartementTest ");
	this.departement.setId(iEntrepriseService.ajouterDepartement(this.departement));
    assertTrue(this.departement.getId() > 0);
	LOG.info(this.mission);
    LOG.info(this.departement);
	Mission mission = TimesheetServiceImpl.affecterMissionADepartement(this.mission.getId(), this.departement.getId());
	Optional<Mission> missionR = missionRepository.findById(this.mission.getId());
    if (missionR.isPresent()) {
        this.mission = missionR.get();
        LOG.debug(MessageFormat.format("Fetched Mission: {0}", this.mission.getId()));
    }
	assertEquals(mission.getDepartement().getId(), this.departement.getId());
		
	}
	
	
	public void ajouterTimesheetTest(){
		LOG.info("Method ajouterTimesheetTest ");
		assertTrue(EmployeServiceImpl.addOrUpdateEmploye(this.employe) > 0);
        assertTrue(EmployeServiceImpl.addOrUpdateEmploye(this.chefDepartement) > 0);
        EmployeServiceImpl.affecterEmployeADepartement(this.chefDepartement.getId(), this.departement.getId());
		Date dateDebut=new Date();
		Date dateFin=new Date();
	    Timesheet timesheet=TimesheetServiceImpl.ajouterTimesheet(this.mission.getId(), this.employe.getId(), dateDebut, dateFin);
	    Iterable<Timesheet> timesheets = timesheetRepository.findAll();
        for (Timesheet ts : timesheets) {
            if ((ts.getMission().getId() == this.mission.getId()) && (ts.getEmploye().getId() == this.employe.getId())) {
                List<Timesheet> timeSheet = new ArrayList();
                timeSheet.add(ts);
                this.mission.setTimesheets(timeSheet);
            }
        }
	    LOG.info(this.mission);
        LOG.info(this.employe);
	assertEquals(timesheet.getTimesheetPK().getIdMission(),this.mission.getId());
	assertEquals(timesheet.getTimesheetPK().getIdEmploye(),this.employe.getId());
	assertEquals(timesheet.getTimesheetPK().getDateDebut(),dateDebut);
	assertEquals(timesheet.getTimesheetPK().getDateFin(),dateFin);
	}
	 
	
	public void validerTimesheetTest(){
        LOG.info("Method validerTimesheetTest ");	
		assertTrue(EmployeServiceImpl.addOrUpdateEmploye(this.chefDepartement) > 0);
		LOG.info(this.mission);
        LOG.info(this.chefDepartement);
		Date dateDebut=new Date();
		Date dateFin=new Date();
		TimesheetServiceImpl.validerTimesheet(this.mission.getId(),this.employe.getId(), dateDebut, dateFin, this.chefDepartement.getId());
        Iterable<Timesheet> timesheets = timesheetRepository.findAll();
        for (Timesheet ts : timesheets) {
            if ((ts.getMission().getId() == this.mission.getId()) && (ts.getEmploye().getId() == this.employe.getId())) {
                List<Timesheet> timesheet = new ArrayList();
                timesheet.add(ts);
                this.mission.setTimesheets(timesheet);
            }
        }
		assertTrue(this.mission.getTimesheets().get(0).isValide());
        LOG.info(this.mission.getTimesheets().get(0));
	}
	
	public void findAllMissionByEmployeJPQLTest() {
        LOG.info("Method findAllMissionByEmployeJPQLTest ");
        List<Mission> missions = TimesheetServiceImpl.findAllMissionByEmployeJPQL(this.employe.getId());
        LOG.info(missions);
        assertTrue(missions.size() > 0);
    }

	public void getAllEmployeByMissionTest() {
        LOG.info("Method getAllEmployeByMissionTest ");
        List<Employe> employes = TimesheetServiceImpl.getAllEmployeByMission(this.mission.getId());
        LOG.info(employes);
        assertTrue(employes.size() > 0);
    }
	
}
