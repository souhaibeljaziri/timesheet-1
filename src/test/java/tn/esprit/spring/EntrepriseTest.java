package tn.esprit.spring;

import java.text.ParseException;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.services.IEntrepriseService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EntrepriseTest {

	private Entreprise entreprise;
	private Departement departement;

	@Autowired
	IEntrepriseService iEntrepriseService;

	@Before
	public void setUp() {
		entreprise = new Entreprise("Vermeg", "software for banking");
		departement = new Departement("Neuchatel");
	}

	@Test
	public void tests() throws ParseException {
		testAjouterEntreprise();
		testGetEntrepriseById();
		testAjouterDepartement();
		testGetAllDepartementsNamesByEntreprise();
		testAffecterDepartementAEntreprise();
		testDeleteEntrepriseById();
		testDeleteDepartementById();
	}

	public void testAjouterEntreprise() {
		int entrepriseId = iEntrepriseService.ajouterEntreprise(entreprise);
		assertEquals(entreprise.getId(), entrepriseId);
	}

	public void testGetEntrepriseById() {
		Entreprise ent = iEntrepriseService.getEntrepriseById(entreprise.getId());
		assertEquals(ent.getId(), entreprise.getId());
	}

	public void testAjouterDepartement() {
		int depId = iEntrepriseService.ajouterDepartement(departement);
		assertEquals(departement.getId(), depId);
		assertThat(depId).isPositive();
	}

	public void testGetAllDepartementsNamesByEntreprise() {
		List<String> deps = iEntrepriseService.getAllDepartementsNamesByEntreprise(entreprise.getId());
		assertNotNull(deps);
	}

	public void testAffecterDepartementAEntreprise() {
		assertThat(departement.getId()).isNotNegative();
		assertThat(entreprise.getId()).isNotNegative();
		iEntrepriseService.affecterDepartementAEntreprise(departement.getId(), entreprise.getId());
	}

	public void testDeleteEntrepriseById() {
		assertThat(entreprise.getId()).isNotNegative();
		iEntrepriseService.deleteEntrepriseById(entreprise.getId());
	}

	public void testDeleteDepartementById() {
		assertThat(departement.getId()).isNotNegative();
		
		iEntrepriseService.deleteDepartementById(departement.getId());
	}

}
